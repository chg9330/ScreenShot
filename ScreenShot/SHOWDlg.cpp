// SHOWDlg.cpp : 实现文件
//

#include "stdafx.h"
#include "ScreenShot.h"
#include "SHOWDlg.h"
#include "afxdialogex.h"


// CSHOWDlg 对话框

IMPLEMENT_DYNAMIC(CShowDlg, CDialogEx)

CShowDlg::CShowDlg(CWnd* pParent /*=NULL*/)
	: CDialogEx(IDD_SHOW_DLG, pParent)
{
	//获取屏幕的宽度
	m_nScreenWidth = GetSystemMetrics(SM_CXSCREEN);
	//获取屏幕的高度
	m_nScreenHeight = GetSystemMetrics(SM_CYSCREEN);
	//获取整个桌面 GetDesktopWindow()->GetDC()
	m_pDesktopDC=GetDesktopWindow()->GetDC();
	//创建一个位图对象
	CBitmap bmp;
	bmp.CreateCompatibleBitmap(m_pDesktopDC, m_nScreenWidth, m_nScreenHeight);

	//创建内存DC
	m_memDC.CreateCompatibleDC(m_pDesktopDC);
	m_memDC.SelectObject(&bmp);

	//复制到内存DC
	m_memDC.BitBlt(0, 0, m_nScreenWidth, m_nScreenHeight,m_pDesktopDC, 0, 0, SRCCOPY);
	//释放
	bmp.DeleteObject();
}

CShowDlg::~CShowDlg()
{
}

void CShowDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialogEx::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CShowDlg, CDialogEx)
	ON_WM_PAINT()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONDBLCLK()
END_MESSAGE_MAP()


// CSHOWDlg 消息处理程序


BOOL CShowDlg::OnInitDialog()
{
	CDialogEx::OnInitDialog();

	// TODO:  在此添加额外的初始化
	//调整整个对话框的大小
	SetWindowPos(&wndTopMost,0,0,m_nScreenWidth,m_nScreenHeight,SWP_NOZORDER|SWP_NOMOVE);
	//初始化
	m_tracker.m_rect.SetRect(0, 0, 0, 0);
	m_tracker.m_nHandleSize = 4;
	m_tracker.m_nStyle = CRectTracker::solidLine | CRectTracker::resizeOutside;


	return TRUE;  // return TRUE unless you set the focus to a control
				  // 异常: OCX 属性页应返回 FALSE
}


void CShowDlg::OnPaint()//绘图消息
{
	CPaintDC dc(this); // device context for painting
	//贴图显示出来
	dc.BitBlt(0,0,m_nScreenWidth,m_nScreenHeight,&m_memDC,0,0,SRCCOPY);
	m_tracker.Draw(&dc);

					   // TODO: 在此处添加消息处理程序代码
					   // 不为绘图消息调用 CDialogEx::OnPaint()
}


void CShowDlg::OnLButtonDown(UINT nFlags, CPoint point)//拖动鼠标
{
	// TODO: 在此添加消息处理程序代码和/或调用默认值
	if (m_tracker.HitTest(point )<0)//如果鼠标点击的是对话框外面
	{
		m_tracker.TrackRubberBand(this, point, TRUE);//从当前位置开始,可以翻转截图
		m_tracker.m_rect.NormalizeRect();
	}
	else //鼠标在对换框里面就可以拖动对话框
	{
		m_tracker.Track(this, point, TRUE);
		m_tracker.m_rect.NormalizeRect();
	}
	Invalidate(FALSE);//刷新这个区域
	CDialogEx::OnLButtonDown(nFlags, point);
}

 
void CShowDlg::OnLButtonDblClk(UINT nFlags, CPoint point)//双击保存文件 放到剪切板中
{
	//生成随机数
	srand((unsigned)time(NULL));
	int n = rand() % 1000;//0--999

	//获取当前时间
	CTime time = CTime::GetCurrentTime();

	CString strImgName;
	strImgName.Format(L"%s%d", time.Format(L"%Y%m%d%H%M%S"),n );

	//截取图片的大小
	int nWidth = m_tracker.m_rect.Width();
	int nHeight = m_tracker.m_rect.Height();
	CFileDialog dlg(FALSE, L"png", strImgName, OFN_HIDEREADONLY, L"PNG(*.png)|*.png|BMP(*.bmp)|*.bmp|JPG(*.jpg)|*.jpg||");
		if (IDCANCEL == dlg.DoModal())//如果点击取消按钮就返回来
		return;

	//创建一张位图
	CBitmap bmp;
	bmp.CreateCompatibleBitmap(&m_memDC,nWidth,nHeight);
	//创建临时内存DC
	CDC tempDC;
	tempDC.CreateCompatibleDC(NULL);
	tempDC.SelectObject(&bmp);
	//复制
	tempDC.BitBlt(0,0,nWidth,nHeight,&m_memDC,m_tracker.m_rect.left,m_tracker.m_rect.top,SRCCOPY);
	//粘贴到剪切板
	OpenClipboard();//打开剪切板
	EmptyClipboard();//清空剪切板
	SetClipboardData(CF_BITMAP,bmp.GetSafeHandle());//将图片放到剪切板
	CloseClipboard();//关闭剪切板
	
	
	//保存到文件

	CImage img;
	img.Attach(bmp);
	img.Save(dlg.GetPathName());
	img.Detach();
	//释放内存
	bmp.DeleteObject();
	tempDC.DeleteDC();
	m_memDC.DeleteDC();
	ReleaseDC(m_pDesktopDC);


	CDialogEx::OnLButtonDblClk(nFlags, point);

	EndDialog(TRUE);//截取屏幕保存后退出
}
